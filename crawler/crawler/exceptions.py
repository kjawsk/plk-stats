class InvalidPipelineArgument(Exception):
    """Error used to raise exception, when invalid item is passed to pipeline"""


class InvalidFibaId (Exception):
    """Error used to raise exception, when match with the specified fiba_id does not exist"""


class InvalidTeamName (Exception):
    """Error used to raise exception, when team with the specified name does not exist in
    database"""

class InvalidTeamPlayer (Exception):
    """Error used to raise exception, when TeamPlayer does not exist in database"""
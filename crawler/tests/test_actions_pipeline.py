"""Tests for players pipeline"""

import datetime
import os
import sys
import django
import pytest
sys.path.append("/home/karol/Projects/plkStats/page/")
os.environ["DJANGO_SETTINGS_MODULE"] = "page.settings"
django.setup()
pytestmark = pytest.mark.django_db

from crawler.pipelines import ActionsPipeline
from crawler.exceptions import InvalidFibaId, InvalidTeamName, InvalidTeamPlayer
from stats.models import (Action, Action_Type, Action_Subtype, Team, Team_Player, Match,
                          Period_Type, Player)


@pytest.fixture
def player():
    return ("John Doe", "J. Doe", "USA", "1990-05-27", "198", "shooting guard")


@pytest.fixture
def team1_name():
    return "Team One"


@pytest.fixture
def team2_name():
    return "Team Two"


@pytest.fixture
def fiba_id():
    return 1


@pytest.fixture(scope='function')
def initialization(team1_name, team2_name, fiba_id, player):
    team1 = Team.objects.create(name=team1_name)
    team2 = Team.objects.create(name=team2_name)
    Match.objects.create(
        home_team=team1,
        away_team=team2,
        date=datetime.datetime.now(),
        fiba_id=fiba_id,
    )
    name, short_name, passport, birth, height, position = player
    player_ = Player.objects.create(
        name=name,
        short_name=short_name,
        passport=passport,
        birth=birth,
        height=height,
        position=position
    )
    Team_Player.objects.create(player=player_, team=team1, to=None)

@pytest.fixture()
def item(fiba_id, team1_name, player):
    def wrapped(fiba_id=fiba_id, team_name=team1_name, action_type="", action_subtype="",
                player_name=player[0], period_type="", time="00:00", success=True, period=1):
        return {
            'fiba_id': fiba_id,
            'team': team_name,
            'action_type': action_type,
            'action_subtype': action_subtype,
            'player_name': player_name,
            'period_type': period_type,
            'time': time,
            'success': success,
            'period': period
        }
    return wrapped


def test_exception_is_raised_when_match_with_fiba_id_does_not_exist(item):
    """Checks if InvalidFibaId is raised, when match with specified fiba id does not exist in db"""
    with pytest.raises(InvalidFibaId) as exc_info:
        ActionsPipeline().process_item(item(), "")
    assert "Match with following fiba_id does not exist in db: 1" in str(exc_info.value)


def test_exception_is_raised_when_teamplayer_does_not_exist(initialization, item, team1_name):
    """Checks if InvalidTeamPlayerName is raised, when TeamPlayer with specified name does not
    exist in db"""
    with pytest.raises(InvalidTeamPlayer) as exc_info:
        ActionsPipeline().process_item(item(player_name="Ty Law"), "")
    assert "TeamPlayer does not exist: \nTeam - %s \nPlayer - Ty Law" %\
           team1_name in str(exc_info.value)


def test_exception_is_raised_when_team_name_does_not_exist(initialization, item):
    """Checks if InvalidTeamName is raised, when team with specified name does not exist in db"""
    with pytest.raises(InvalidTeamName) as exc_info:
        ActionsPipeline().process_item(item(team_name="Anwil"), "")
    assert "Team does not exist: Anwil" in str(exc_info.value)


def test_action_type_is_created(initialization, item):
    """Checks if Action_Type object is created"""
    ActionsPipeline().process_item(item(action_type="3pkt"), "")

    assert Action_Type.objects.filter(name="3pkt").exists()


def test_action_subtype_is_created(initialization, item):
    """Checks if Action_Subtype object is created"""
    ActionsPipeline().process_item(item(action_subtype="jumpshot"), "")

    assert Action_Subtype.objects.filter(name="jumpshot").exists()


def test_period_type_is_created(initialization, item):
    """Checks if Period_Type object is created"""
    ActionsPipeline().process_item(item(period_type="REGULAR"), "")

    assert Period_Type.objects.filter(name="REGULAR").exists()


def test_action_is_created(initialization, item, fiba_id, team1_name, player):
    """"Checks if Action object is created"""
    ActionsPipeline().process_item(item(
        fiba_id=fiba_id,
        team_name=team1_name,
        action_type="2pkt",
        action_subtype="dunk",
        player_name=player[0],
        period_type="REGULAR",
        time="02:34",
        success=True,
        period=2
    ), "")

    assert Action.objects.filter(
        match__fiba_id=fiba_id,
        teamplayer__player__name=player[0],
        teamplayer__team__name=team1_name,
        action_type__name="2pkt",
        action_subtype__name="dunk",
        time="02:34",
        success=True,
        period_type__name="REGULAR",
        period=2,
    ).exists()

